import cv2
import os

''' This script extracts video frames from an input video, saving the individual frames as .JPGs'''

def createFolder(directory):
  
  try:

    if not os.path.exists(directory):
      os.makedirs(directory)

  except OSError:

    print('Error: Creating directory. ' + directory)
  return directory


filename ='candle_open_normal_1.avi'
vidcap = cv2.VideoCapture('candle/candle_open_normal_1.avi')
success,image = vidcap.read()
count = 0
while success:
  #cv2.imwrite("/home/ridgei27/Downloads/Namako-ochi-20181205T080838Z-001/Namako-ochi/Neg/"+filename+"rame%d.jpg" % count, image)     # save frame as JPEG file
  Outputput = createFolder("output/" + filename+ "/")

  cv2.imwrite(
    "output/" + filename+ "/2cframe%d.jpg" % count,
    image)  # save frame as JPEG file
  success,image = vidcap.read()
  print('Read a new frame: ', success)
  count += 1
