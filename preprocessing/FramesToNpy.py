"""
New version of preprocessing for clstmae on SNT dataset.
Dataset mean normalization has been dropped to reduce computation time, and only sequences are saved - not frames.
"""

import os
from glob import glob
import argparse

import numpy as np
import skimage

# Parameters
# ap = argparse.ArgumentParser()
# ap.add_argument('-f', '--frames', help="Path of folder containing raw frames.")
# ap.add_argument('-d', '--destination', required=True, help="Path of folder storing the output sequences.")
# ap.add_argument('-T', required=False, type=int, default=15, help="Number of frames in a sequence.")
# ap.add_argument('-o', '--one-dir', required=True,
#   help="True: put all generated sequences in the same folder.\n False: create one output folder for each video.", type=bool)

# args = vars(ap.parse_args())
# raw_frames_folder = args["frames"]
# sequences_path    = args["destination"]
#T = int(args["T"])
#target_size = (123, 123)

raw_frames_folder = "/home/baaron/Projectbrary/fire_promotion/data/frames/"
sequences_path    = "/home/baaron/Projectbrary/fire_promotion/data/train/"
target_size = (83, 83)
T = 15
one_dir     = False

def img_preprocessing(id_lst, out_path, T=15):
    """
    Modify the frames according to the paper, and fill them into a sequence object that is saved in out_path.
    """
    #assert id_lst == True
    
    img_lst = np.zeros((T, 1) + target_size)
    #print(id_lst[0])
    vid_name = id_lst[0].split('/')[-2]
    count = 0
    for i, img_path in enumerate(id_lst):
        img = skimage.io.imread(img_path).astype(float)

        # Convert to grayscale
        img = skimage.color.rgb2gray(img)

        # Resize to target size
        img = skimage.transform.resize(img, target_size)

        # Rescale to [0, 1]
        img /= 255.

        # Normalize to N(0, 1)
        img = (img - np.mean(img)) / np.std(img)

        # Resize to 1 x size x size
        img = np.resize(img, (1,) + target_size)

        # Add to sequence
        img_lst[i%T, 0, :, :] = img

        # When sequence is full, save it
        if i >= T-1:
            out_name  = os.path.join(out_path, vid_name + '_' + str(count))
            nan_count = np.count_nonzero(np.isnan(img_lst))

            print('Saving sequence ' + out_name + ". Found ", end='\r')

            if nan_count == 0:
                np.save(out_name, img_lst)
                count += 1
            else:
                print("Found {} NaN values. Skipping......".format(nan_count))
            

if __name__ == '__main__':
    vid_lst = sorted(glob(os.path.join(raw_frames_folder, '*')))
    #print(vid_lst)
    if not vid_lst:
        " Video list is empty! "

    if not os.path.exists(sequences_path):
        os.mkdir(sequences_path)

    # Preprocessing
    for vid_path in vid_lst:
        vid_name = vid_path.split('/')[-1]
        print('Processing {}'.format(vid_name))
        #frames = sorted(glob(os.path.join(vid_path, '*.png')))
        frames = sorted(glob(os.path.join(vid_path, '*.jpg')))

        # Training mode: all the sequences are placed in the same dir
        if one_dir:
            img_preprocessing(frames, out_path=sequences_path, T=T)

        # Evaluation mode: each directory contains sequences from the same video
        else:
            out_path = os.path.join(sequences_path, vid_name)
            os.makedirs(out_path, exist_ok=True)
            img_preprocessing(frames, out_path=out_path, T=T)
