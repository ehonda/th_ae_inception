"""
Convolutional Long Short Term Memory AutoEncoder for Anomaly detection.
This network takes stacked frames as input, and attempt to reconstruct them by
minimizing the euclidean loss.
Architecture described at https://arxiv.org/abs/1701.01546
"""

import os
from glob import glob
from random import shuffle
import json
import argparse
from time import time

import tensorflow as tf
import keras
import keras.backend as K
from keras.utils.training_utils import multi_gpu_model

from generator import DataGenerator

def euclidean_dist(x, y):
    return K.sqrt(K.sum(K.square(x - y)))

class CheckpointPar(keras.callbacks.Callback):
    def __init__(self, model):
        self.model_to_save = model

    def on_epoch_end(self, epoch, logs=None):
        #self.model_to_save.save('models/model_e{}.h5'.format(epoch + 1))
        self.model_to_save.save('/home/baaron/Projectbrary/fire_promotion/models/model_e{}.h5'.format(epoch + 1))

# parser = argparse.ArgumentParser()
# parser.add_argument('training_path', help='Path to the folder containing training data.')
# parser.add_argument('T', type=int, help='Length of an input sequence.')
# parser.add_argument('-b', '--batch', type=int, required=False, default=32, help='Batch size for training.')
# args = vars(parser.parse_args())

# # Parameters
# TRAINING_PATH = args['training_path']
# T             = args['T']
# BATCH_SIZE    = args['batch']

# # Parameters
# TRAINING_PATH = '/home/baaron/Projectbrary/fire_promotion/data/train/camp_fire_2hr_sample/'
TRAINING_PATH = '/workspace/phase2/CLSTM-AE/train_data/'
T             = 15
BATCH_SIZE    = 16

params = {'dim'        : (1, 83, 83),
          'batch_size' : BATCH_SIZE,
          'n_channels' : T,
          'shuffle'    : True}


# Multiple GPUs
with tf.device('/cpu:0'):
#with tf.device('/gpu:0'):
    model = keras.Sequential()

    # Spatial encoder: 2 time-stacked convolutional layers
    model.add(keras.layers.TimeDistributed(keras.layers.Conv2D(128, (11, 11), padding='valid',
        activation  = 'tanh', strides = (4, 4), kernel_regularizer = 'l2', bias_regularizer = 'l2',
        data_format = 'channels_first'), input_shape = (params['n_channels'],) + params['dim']))

    model.add(keras.layers.TimeDistributed(keras.layers.Conv2D(64, (5, 5), padding='valid',
        activation  = 'tanh', strides = (2, 2), kernel_regularizer='l2', bias_regularizer='l2')))

    # Temporal encoder-decoder: 3 ConvLSTM layers
    model.add(keras.layers.ConvLSTM2D(64, (3, 3), padding='same', return_sequences=True, kernel_regularizer='l2', bias_regularizer='l2'))
    # model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.ConvLSTM2D(32, (3, 3), padding='same', return_sequences=True, kernel_regularizer='l2', bias_regularizer='l2'))
    # model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.ConvLSTM2D(64, (3, 3), padding='same', return_sequences=True, kernel_regularizer='l2', bias_regularizer='l2'))
    # model.add(keras.layers.BatchNormalization())

    # Spatial decoder: 2 time-stacked deconvolutional layers
    model.add(keras.layers.TimeDistributed(keras.layers.Conv2DTranspose(128, (5, 5), padding='valid',
        activation='tanh', strides=(2, 2), kernel_regularizer='l2', bias_regularizer='l2')))
    model.add(keras.layers.TimeDistributed(keras.layers.Conv2DTranspose(1, (11, 11), padding='valid',
        activation='tanh', strides=(4, 4), kernel_regularizer='l2', bias_regularizer='l2')))

model.compile(optimizer='adam', loss=euclidean_dist)

model_par = multi_gpu_model(model, gpus=2)
#model_par = model
model_par.compile(optimizer='adam', loss=euclidean_dist)

# Datasets
#inputs_search_path
#inputs = glob(os.path.join(TRAINING_PATH, '*', '*.npy'))
inputs = glob(os.path.join(TRAINING_PATH, '*.npy'))
shuffle(inputs)
N_train = len(inputs)

if N_train == 0:
    print("Nothing in the data input list!! ")
    
partition               = {}
partition['train']      = inputs[:int(N_train * 0.9)]
partition['validation'] = inputs[int(N_train * 0.9):]
# partition['train'] = inputs[:1000]
# partition['validation'] = inputs[1000:1100]

# Generators
training_generator   = DataGenerator(partition['train'], **params)
validation_generator = DataGenerator(partition['validation'], **params)


# Callbacks
# checkpoint = keras.callbacks.ModelCheckpoint('models/model.{epoch:02d}_{val_loss:.2f}', monitor='val_loss')
checkpoint_par = CheckpointPar(model_par.get_layer('sequential_1'))
earlyStop      = keras.callbacks.EarlyStopping(patience=10)
tensorboard    = keras.callbacks.TensorBoard(log_dir='./logs/logs{}'.format(time()))

# Training
# WARNING: Loss may suddenly jumpy to 'inf' or 'nan' if training data contains an 'nan' values
# This can happen even if initially the loss seems to be decreasing.
#
# Depending on input data size, batch_size may need to be decreased to avoid GPU ram overload.
# This is usually indicated by an OMM 'Resources exhausted' message.

hist = model_par.fit_generator(generator=training_generator,
                        validation_data=validation_generator,
                        epochs  = 50,
                        use_multiprocessing=True,
                        workers = 8,
                        callbacks=[checkpoint_par, earlyStop, tensorboard])

with open('hist.json', 'w') as f:
    json.dump(hist.history, f)
