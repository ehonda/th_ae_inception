"""
Data generator for Keras.
"""
import numpy as np
import keras

class DataGenerator(keras.utils.Sequence):
    'Generate data for Keras'
    def __init__(self, list_IDs, batch_size=32, dim=(32, 32, 32), n_channels=1, shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.shuffle = shuffle
        self.on_epoch_end()

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, n_channels, *dim)
        # Initialization
        X = np.empty((self.batch_size, self.n_channels, *self.dim))

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store samples
            X[i,] = np.load(ID)
        return X
    
    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X = self.__data_generation(list_IDs_temp)
        return X, X             # Our model is an autoencoder
        