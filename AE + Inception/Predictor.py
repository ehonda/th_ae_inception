
RANDOM_SEED = 42
import random
random.seed(RANDOM_SEED)
import numpy as np
np.random.seed(RANDOM_SEED)

from keras.models import load_model
from keras.backend.tensorflow_backend import set_session
import tensorflow as tf
import cv2
import utils

Classes = ['Clear',       'Namako',   'Stagnation', 
           'No Bifuntan', 'Breakage', 'Na+Bi',  
           'Stag+Bi',     'St+Na+Bi', 'Bright Namako']   # List of classes

ModelPath = './Models/'

TrainingDim, TrainingDim = (32, 32) # These should match the TrainingDim used during training of the input Inception model.
TARGET_SIZE              = (83, 83)
TimeWindow               = 15
# Loss function used for STAE

def Predictor(InputPath, inception_weights = './Models/Inception/Inception.h5'):

    '''The predictor function that executes a video against STAE and Inception model. 
    Takes a video path as input, returns the frame as an OpenCv image, the grayscaled frame, 
    the frame index, the regularity score, and the class.
    '''
    # Limit keras memory usage.
    # Not doing so may cause all GPU memory to be quickly consumed, and system to become unstable.

    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 0.20
    set_session(tf.Session(config=config))

    ImgCtr = 0    # the frame counter registering frame index
    vidcap = cv2.VideoCapture(InputPath)


    frame_number     = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    yield frame_number

    # READING VIDEO
    success, image   = vidcap.read()
    ProcessingWindow = []
    stacked_frames   = np.zeros((1, TimeWindow, 1) + TARGET_SIZE)
    RegScore, Class  = 0, 'N/A'

    # Loading trained models for STAE AND Inception. These are trained separately and then copied into the O

    #Inception_model  = load_model(ModelPath + 'Inception/Inception.h5') # loads the inception model --- default location for the Phase 2 results
    Inception_model = load_model(inception_weights) # Allow to specify via cmd arg fed into online_viewer.py
    STAE_model      = load_model(ModelPath + 'STAE/STAE.h5', custom_objects={'euclidean_dist': utils.euclidean_dist}) # loads the STAE model with euclidean loss -- as was used in Phase 2 and 3 results.
    #STAE_model       = load_model(ModelPath + 'STAE/STAE.h5', custom_objects={'mixed_loss': utils.mixed_loss})         # mixed loss function (not used since early-mid Phase 2)


    while success:
        # the preprocessing required for the STAE model
        frame      = image
        frame      = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        image_gray = frame
        frame      = cv2.resize(frame, TARGET_SIZE).astype('float32')
        frame     /= 255.0
        frame      = (frame - np.mean(frame)) / (np.std(frame) + 0.00001)

        if ImgCtr < TimeWindow:
            stacked_frames[0, ImgCtr, 0, :, :] = frame

            # If stacked_frames is full, shift every element by -1 and add the new frame at the end.
        else:
            stacked_frames = np.roll(stacked_frames, -1, axis=1)
            stacked_frames[0, -1, 0, :, :] = frame
            # Evaluate the model on the current sequence
            RegScore =STAE_model.evaluate(stacked_frames, stacked_frames, verbose=0)

        # the preprocessing required for INCEPTION
        img          = cv2.resize(image, (TrainingDim, TrainingDim), interpolation=cv2.INTER_NEAREST)
        CurrentImage = img.astype('float32') / 255.0, 0

        # Append values until the length equals TimeWindow
        if len(ProcessingWindow) != TimeWindow:
            ProcessingWindow.append(np.reshape(CurrentImage[0], [1, TrainingDim, TrainingDim, 3]))
        else:
            # Evaluate the current sequance
            preds = Inception_model.predict(ProcessingWindow)
            Class = Classes[np.argmax(preds[0])]

            # Shift the array one step backward and add the latest frame to the end
            ProcessingWindow     = np.roll(np.asarray(ProcessingWindow), -1, axis=0)
            ProcessingWindow     = [ProcessingWindow[i, :, :] for i in range(TimeWindow)]
            ProcessingWindow[-1] = np.reshape(CurrentImage[0], [1, TrainingDim, TrainingDim, 3])

        image       = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        if ImgCtr  >= TimeWindow:
            Results = (image, image_gray, ImgCtr, RegScore, Class, preds[0])
            yield Results

        ImgCtr = ImgCtr + 1
        success, image = vidcap.read()
