'''
Runs input videos at assumed 15 FPS through the STAE and Inception models.
Frame-by-frame, this script produces the model outputs and synthesizes a
"dashboard" video output showing STAE reg. score progression along with
Inception top-class label, alongside the input video itself.

Finally the model outputs are saved into a .CSV file and overview plots 
are written as .PNGs.
'''
import argparse
import os
import subprocess
import time
import datetime
from glob import glob
from multiprocessing import Pool
import matplotlib
#matplotlib.use("Qt4Agg")
matplotlib.use("Agg")
#matplotlib.use("tkagg")
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from Predictor import Predictor
import utils


plt.style.use('dark_background')
BASE_DIR               = '../../../snt_data/'   
IN_VID                 = None

PHASE                  = 3 #3
PLANT                  = "C"
EXPERIMENT_NAME        = 'Case_B7'


if PLANT == "C":
    PHASE            = 3
    EXPERIMENT_NAME  = "Case_A"
elif PLANT == "B":
    PHASE            = 3

# IN_FOLDER             = '{}TestingData/phase{}/Plant{}'.format(BASE_DIR, PHASE, PLANT) 
IN_FOLDER             = 'video'
# OUT_FOLDER            = '../../../snt_deliverables/Output/phase{}/Plant{}/{}'.format(PHASE, PLANT,EXPERIMENT_NAME)
OUT_FOLDER            = 'result'
FIG_DISPLAY           = False

if EXPERIMENT_NAME == "Case_A":
    # INCEPTION_MODEL_PATH = './Models/TI/TI.h5'
    INCEPTION_MODEL_PATH = './Models/Inception/Inception.h5'
else:
    INCEPTION_MODEL_PATH = BASE_DIR + 'Experiments/phase3_tuning/{}/ModelOutputs/Puremodel.h5'.format(EXPERIMENT_NAME)

WHICH_GPU            = '1'

# Restrict to 1 GPU
os.environ["CUDA_VISIBLE_DEVICES"] = WHICH_GPU

if IN_VID is None:
    IN_VID = sorted(glob(os.path.join(IN_FOLDER, '*.avi')))
os.makedirs(OUT_FOLDER, exist_ok=True)

if os.path.isdir(IN_FOLDER) == False:
    raise ValueError(" Directory {} does not exist-- model output will fail. Did you forget to make it?".format(IN_FOLDER))

if os.path.isdir(OUT_FOLDER) == False:
    raise ValueError(" Directory {} does not exist-- dataloading will fail. Did you forget to make it?".format(OUT_FOLDER))

if os.path.exists(INCEPTION_MODEL_PATH) == False:
    raise ValueError(" Model path {} does not exist-- dataloading will fail. Did you forget to make it?".format(INCEPTION_MODEL_PATH))



T = 15
ModelPath = './Models/'
colors = { 'Clear'        : 'green', 'Namako'      : 'red', 
           'Stagnation'   : 'blue',  'No Bifuntan' : 'purple',
           'Breakage'     : 'black', 'St+Na'       : 'black', 
           'Bright Namako': 'orange'}


csv_columns_model  =  ['class',         'bifuntan?',       'Clear prob', 
                       'Namako prob',   'Stagnation prob', 'No Bifuntan prob', 
                       'Breakage prob', 'Na+Bi prob',      'Stag+Bi prob', 
                       'St+Na+Bi prob' ]

csv_columns_stats  =  [ 'mean',     'std',    'skew',
                        'kurt',     'median', 'mode']
                        
csv_columns_hist   =  [i for i in range(0, 256)]
csv_columns        =  ["time"] + csv_columns_model + csv_columns_stats + csv_columns_hist



def evaluate(input_name):

    '''Retrieved per frame results for model inference --- both Inception and STAE, 
    the post-processed `No Bifuntan` label, and simple brightness statcs (mean, median, mode, kurtosis, skew, and histogram.)

    input_name : Either a full path to an input video file to be inferenced, or a folder containing multiple videos.
    '''

    # os.environ["CUDA_VISIBLE_DEVICES"] = gpu_idx
    OUT_VID = os.path.join(OUT_FOLDER, 'result_' + os.path.basename(input_name))
    OUT_CSV = os.path.join(OUT_FOLDER, 'raw_' + os.path.basename(input_name).split('.')[-2] + '.csv')
    OUT_FIG = os.path.join(OUT_FOLDER, 'fig_' + os.path.basename(input_name).split('.')[-2] + '.png')



    df = pd.DataFrame(columns=csv_columns)

    rec_loss = []

    # Create the generator for results
    results_gen  = Predictor(input_name, inception_weights=INCEPTION_MODEL_PATH)
    VID_LEN      = next(results_gen) - T
    first_output = next(results_gen)

    # Reset the generator
    results_gen = Predictor(input_name, inception_weights=INCEPTION_MODEL_PATH)
    next(results_gen)

    first_img = first_output[0]
    rmin      = 0
    # bifun_presence = True
    bifun_man = utils.BifunManager(doubt_threshold=45)

    # # Create the figure
    fig = plt.figure(dpi=100)
    canvas_width, canvas_height = fig.canvas.get_width_height()

    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212)
    ax2.set_xlim(0, VID_LEN)
    ax2.set_ylim(0.8, 1)
    ax2.set_facecolor((1, 1, 1))

    # #Fig init
    img      = ax1.imshow(first_img)
    x        = np.arange(VID_LEN)
    line1,   = ax2.plot(0, 1, color='blue')
    line2,   = ax2.plot([0, 0], [1, 1], color='orange', alpha=0.3)
    label    = ax2.text(0, 0.98, "NA", color='black')
    bifunlab = ax2.text(VID_LEN, rmin, "No bifuntan", color='white', horizontalalignment='right')
    fig.canvas.draw()
    if FIG_DISPLAY:
        plt.show(block=False)


    def update(result, df):

        ''' Updates the figure initiated above: First, fetch results from the generator for the latest frame.
        Results are then appended to both the figure and the data frame, which will later be saved as a CSV.

        result : The instantiated generator for the video inference results
        df     : The Pandas df to which results will be appended.
        '''

        # Iterate the generator and obtain results from the latest frame
        # Since the generator at present does not conduct any brightness statistics calculations,
        # we return the grayscale image frame along with the RGB, so that we can calculate brightness stats in this step.
        # WARNING: This order of operations could be mess less efficient than doing brightness stats within the generator itself.
        # Future development should consider doing just that....
        
        frame, image_gray, im_count, loss, \
            class_res, class_confs = result

        class_res, bifun_presence  = utils.bifuntan_management(class_res)
        bifun_print = not bifun_man.observe(bifun_presence)
        i           = im_count - T
        rec_loss.append(loss)
        reg_scores  = utils.compute_reg_score(rec_loss)
        rmin        = min(reg_scores)

        img.set_data(frame)
        #ax2.set_ylim(rmin, 1)
        ax2.set_ylim(bottom=rmin, top=1)
        line1.set_data(x[:i], reg_scores[:i])
        line2.set_data([i, i], [rmin, 1])
        label.set_text(class_res)
        label.set_color(colors[class_res])
        bifunlab.set_y(rmin)
        if i < 0.8 * VID_LEN:   
            label.set_x(i)
            label.set_y(rmin + 0.9 * (1 - rmin))
        else:
            label.set_horizontalalignment('right')
            label.set_x(i)
            label.set_y(rmin + 0.9 * (1 - rmin))

        if bifun_print:
            bifunlab.set_color(colors['No Bifuntan'])
        else:
            bifunlab.set_color('white')

        # Update the dataframe

        vals_to_append_model = {    'class'           : class_res,       'bifuntan?'        : not bifun_print, 
                                    'Clear prob'      : class_confs[0],  'Namako prob'      : class_confs[1],
                                    'Stagnation prob' : class_confs[2],  'No Bifuntan prob' : class_confs[3], 
                                    'Breakage prob'   : class_confs[4],  'Na+Bi prob'       : class_confs[5], 
                                    'Stag+Bi prob'    : class_confs[6],  'St+Na+Bi prob'    : class_confs[7],
                                    'Bright Namako'   : class_confs[8]}
                
        vals_to_append_stats, vals_to_append_hist  = utils.calc_stats(image_gray)


        time_sec = ((im_count) // 15)-1
        time     = str(datetime.timedelta(seconds=time_sec))
        vals_to_append_time  = {'time':time}

        time_sec = ((im_count) // 15)-1
        time     = str(datetime.timedelta(seconds=time_sec))
        vals_to_append_time  = {'time':time}
        
        vals_to_append       = {**vals_to_append_time,**vals_to_append_model, **vals_to_append_stats, **vals_to_append_hist}

        df = df.append(vals_to_append, ignore_index=True)

        return df

    if not FIG_DISPLAY:
        # Create a ffmpeg process
        cmdstring = ('ffmpeg',
                    '-y', '-r', '15',
                    '-s', '%dx%d' % (canvas_width, canvas_height),
                    '-pix_fmt', 'argb',
                    '-f', 'rawvideo', '-i', '-',
                    '-c:v', 'mjpeg', '-q:v', '5', '-pix_fmt', 'yuvj420p', OUT_VID)
        p = subprocess.Popen(cmdstring, stdin=subprocess.PIPE)

    # Draw frame and write to the pipe
    curr_time = time.time()
    for result in results_gen:
        # draw the frame
        df = update(result, df)
        ax1.draw_artist(img)
        ax2.draw_artist(ax2.patch)
        ax2.draw_artist(ax2.yaxis)
        ax2.draw_artist(line1)
        ax2.draw_artist(line2)
        ax2.draw_artist(label)
        ax2.draw_artist(bifunlab)
        #fig.canvas.update()
        fig.canvas.draw()
        fig.canvas.flush_events()
        if not FIG_DISPLAY:
            # extract the frame as an ARGB string
            string = fig.canvas.tostring_argb()
            # write to the pipe
            p.stdin.write(string)
        else:
            time_diff = time.time() - curr_time
            curr_time = time.time()
            print('Current FPS: {}'.format(1/time_diff), end='\r')

    if not FIG_DISPLAY:
        # Finish up
        p.communicate()

    # Add the reg scores to the dataframe
    reg_scores = utils.compute_reg_score(rec_loss)

    df = df.assign(reg=reg_scores)

    df = df[ ["time", "reg"] + csv_columns_model + csv_columns_stats + csv_columns_hist ]

    # Save to csv file
    df.to_csv(OUT_CSV, sep=',', index_label='frame')

    # Draw output figure and plot
    utils.plot_results(df, OUT_FIG)

# Multiprocessing version
pool = Pool(4)
#pool.map(evaluate, IN_VID[1::3])
pool.map(evaluate, IN_VID)

os.system('cp online_viewer.py {}/online_viewer.py'.format(OUT_FOLDER))
os.system('cp Predictor.py {}/Predictor.py'.format(OUT_FOLDER))
os.system('cp utils.py {}/utils.py'.format(OUT_FOLDER))

# Linear version (for profiling)
#evaluate(IN_VID[0])
#evaluate(IN_VID)
