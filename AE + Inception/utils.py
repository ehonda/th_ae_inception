RANDOM_SEED        = 42
import random
random.seed(RANDOM_SEED)
import numpy as np
np.random.seed(RANDOM_SEED)
import matplotlib.pyplot as plt
import matplotlib
import keras.backend as K
import tensorflow as tf

import scipy.stats
from scipy.stats import skew as Skew
from scipy.stats import kurtosis as Kurt
from scipy.stats import mode as Mode


colors = {'Clear'         : 'green', 'Namako'      : 'red', 
          'Stagnation'    : 'blue',  'No Bifuntan' : 'purple',
          'Breakage'      : 'black', 'St+Na'       : 'black', 
          'Bright Namako' : 'orange'}

class BifunManager():
    
    def __init__(self, doubt_threshold):
    
        self.state       = 1
        self.observation = 1
        self.doubt       = doubt_threshold
        self.threshold   = doubt_threshold

    
    def observe(self, bifun_presence):

        # Update the observation
        self.observation = bifun_presence

        # update state according to observation and doubt
        if self.observation == self.state:
            self.decrement_doubt()
        else:
            if self.doubt < self.threshold:
                self.doubt += 1
            else:
                self.state = 1 - self.state
                self.doubt = 0

        return bool(self.state)

    def decrement_doubt(self):
        if self.doubt > 0:
            self.doubt -= 1
        else:
            self.doubt = 0

def plot_results(res_df, out_fig_name):

    ''' Classes plotting'''

    reg_scores  = np.array(res_df['reg'])
    x           = np.arange(len(reg_scores))
    class_masks = dict.fromkeys(colors.keys())
    to_delete   = []

    for key in class_masks:
        idx = np.array(res_df[res_df['class'] == key]['reg'].index)
        
        if idx.size == 0:
            to_delete.append(key)
            continue
        
        diff             = np.diff(idx)
        to_append        = np.where(diff != 1)[0]
        insert           = idx[to_append] + 1
        fixed_idx        = np.insert(idx, to_append + 1, insert)
        y                = reg_scores.copy()
        y[fixed_idx]     = 99
        class_masks[key] = np.ma.masked_where(y != 99, y).mask

    for key in to_delete:
        del class_masks[key]

    # Bifuntan indicator plotting
    bifun_idx  = np.array(res_df[res_df['bifuntan?'] == False].index)
    bifun_diff = np.diff(bifun_idx)
    boundaries = np.where(bifun_diff != 1)[0]
    beg_idx    = []
    widths     = []

    if bifun_idx.size != 0:
        beg_idx.append(bifun_idx[0])

        for bound_idx in boundaries:
            widths.append(bifun_idx[bound_idx] - beg_idx[-1])
            beg_idx.append(bifun_idx[bound_idx + 1])
        
        widths.append(bifun_idx[-1] - beg_idx[-1])

    # Plotting
    plt.style.use('default')
    plt.rcParams.update({'font.size': 30})
    _, ax = plt.subplots(1, 1, figsize=(30, 15))
    ax.set_facecolor((1, 1, 1))
    ax.set_xlim(0, len(reg_scores))
    ax.set_ylim(min(reg_scores), 1)
    ax.set_xlabel('Frame #')
    ax.set_ylabel('Regularity Score')

    for key, mask in class_masks.items():
        ax.plot(np.ma.masked_where(mask, x), np.ma.masked_where(mask, reg_scores), color=colors[key], label=key) 

    for i, beg in enumerate(beg_idx):
        ax.add_patch(plt.Rectangle((beg, min(reg_scores)), width=widths[i], height=1-min(reg_scores),
                                        fill=True, color='purple', alpha=0.1))
        ax.text(beg, min(reg_scores), 'Purge', verticalalignment='bottom', color='purple')

    plt.legend()
    plt.savefig(out_fig_name)
    matplotlib.rc_file_defaults()
    plt.style.use('dark_background')


def compute_reg_score(losses):

    '''Given a list of loss functions, compute the regularity score based on 
       the regularity maximum over the current video.'''

    minLoss, maxLoss = np.min(losses), np.max(losses)
    reg_score        = 1 - (losses - minLoss) / maxLoss

    return reg_score

def bifuntan_management(in_class):
    
    if in_class in ('Na+Bi', 'Stag+Bi', 'St+Na+Bi', 'No Bifuntan'):
        bifun_presence = False
        if in_class == 'Na+Bi':
            return 'Namako', bifun_presence
        if in_class == 'Stag+Bi':
            return 'Stagnation', bifun_presence
        if in_class == 'St+Na+Bi':
            return 'St+Na', bifun_presence
        return 'Clear', bifun_presence
    bifun_presence = True

    return in_class, bifun_presence

def euclidean_dist(x, y):

    ''' The Euclidean loss function currently in use by STAE, and upon which Phase 2 and 3 STAE results are based.

    x : Logits produced by forward propagation, i.e. the encoded video frame.
    y : Labels, or in this case the actual input video frame. '''

    return K.sqrt(K.sum(K.square(x - y)))

def gradient_loss(x, y):

    ''' A simple gradient loss function which was part of the experimentation phase for STAE.
    Model development concluded on Euclidean loss as the most effective strategy, this `hase 2 and 3 results 
    do not utilize gradient loss. '''

    return K.sum(K.abs(K.abs(x - tf.manip.roll(x, -1, -2) - K.abs(y - tf.manip.roll(y, -1, -2)))) + \
           K.abs(K.abs(x - tf.manip.roll(x, -1, -1)) - K.abs(y - tf.manip.roll(y, -1, -1))))

def mixed_loss(x, y):

    ''' An experimental hybrid loss, attempting to use both gradient loss and Euclidean loss on measuring frame reconstruction error.
    Not used in Phase 2 and 3 results but kept here as reference and for potential future experimentation'''

    return euclidean_dist(x, y) + gradient_loss(x, y)

def calc_stats(frame):

    '''Given an input video frame (or any array), calculate brightness statistics:
        Mean, median, std, skew, kurtosis, and histogram. Mode is taken as the maximum bin
        value of the histogram, since the frame is integer-valued. Histogram is a 256 bin histogram, with bin-withs of 1.
        Intended for images with integer pixel intensity values ranging form 0 to 255. Histogram will be returned
        as a dictionary where the keys are the left edges of the bins, and values are the counts. Statistics will be returned as a
        separate dictionary. See `calc_hist` for a standalone histogram calculation. 
        
    frame : numpy integer array (Single OpenCV video frame, in context of this project.)'''
    
    # Produce the histogram dictionary
    n_bins      = 256
    hist, bins  = np.histogram(frame, bins = n_bins, range = (0, n_bins)) 
    #hist_dict   = dict(bins, hist)
    bins        = bins[:-1]
    
    hist_dict   = {keys:vals for keys, vals in zip(bins, hist)}

    # Produce the brightness statistics
    mean     = np.mean(frame)
    median   = np.median(frame)
    std      = np.std(frame, ddof=1)
    skew     = Skew(frame, axis=None, bias=False) 
    kurt     = Kurt(frame, axis=None, bias=False)
    #mode     = Mode(np.rint(frame), nan_policy='omit', axis=None)[0][0]
    mode     = bins[hist==hist.max()][0] # Assumes we've already calculated a histogram above.



    stats_dict = {'mean'   : mean,   'std'    : std,
                    'median' : median, 'skew'   : skew,
                    'kurt'   : kurt,   'mode'   : mode }

    return stats_dict, hist_dict

def calc_hist(frame):

    '''Given an input video frame (or any array), calculate a 256 bin histogram, with bin-withs of 1.
    Intended for images with integer pixel intensity values ranging form 0 to 255. Results will be returned
    as a dictionary where the keys are the left edges of the bins, and values are the counts. This function
    is not used on its own in the lates project phase. Same functionality instead built in to `calc_stats`.
    
    frame : numpy integer array (Single OpenCV video frame, in context of this project.)'''

    n_bins      = 256
    hist, bins  = np.histogram(frame, bins = n_bins, range = (0, n_bins)) 
    hist_dict   = {keys:vals for keys, vals in zip(bins[:-1], hist)}

    return hist_dict       
