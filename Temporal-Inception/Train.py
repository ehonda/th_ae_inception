'''
This script was developed to create a classifier for SNT to analyze the furnace and its anomalies.
The development of this script started in early Dec and it was shifted from an anomaly detection to an anomalies classier.

For training sets on par with our initial training conditions for SNT/phase2, meaning = 300MB of training data,
it is HIGHLY RECOMMENDED to run this on a machine with at minimum 32GB of RAM.

Mind also that this version does not read images in sequence. It can be considered a control case wherein a temporal inception
architecture is used, but without feeding temporally sequenced images.

'''
RANDOM_SEED        = 42 # Requiring all of Phase 3 training and inference to use a common random seed for all experiments. Vary this to assess impact of random shuffling.
import os
os.environ["CUDA_DEVICE_ORDER"]    = "PCI_BUS_ID"
# The GPU id to use, usually either "0" or "1"
os.environ["CUDA_VISIBLE_DEVICES"] = "0" # Trains quickly on an NVIDA GTX 1080i (~30 minutes / 20 epochs)
                                         # WARNING This code was not written or tested with dual GPUs in mind (however adaptation may be possible)
                                         # Use caution when running at the same time as inference code, even on separate GPUs.

import cv2
import random
random.seed(RANDOM_SEED)
import numpy as np
np.random.seed(RANDOM_SEED)
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Input
from keras.utils import np_utils
import keras
from keras.models import Model
from keras.optimizers import SGD
from keras.layers import Flatten, Dense

#import pickle
import hickle
import matplotlib.pyplot as plt
from skimage.transform import rescale, resize, downscale_local_mean

import re
import pandas as pd


SORT               = True

BASE_DIR           = '/home/baaron/Projectbrary/snt_data/Experiments/phase3_tuning'
EXPERIMENT_NAME    = 'Case_B9' # Predefined experiment name pointing to existing experiment folder
BATCH_SIZE         = 16    # phase2 used a batch size of 16.
N_EPOCHS           = 20
OPTIMIZER          = 'SGD'
TrainingDatapath_A = '{}/{}/TrainingClasses_A/'.format(BASE_DIR, EXPERIMENT_NAME)  # Subfolder containing training data from 1st data source (Phase 2 "original" Plant A data @ 15FPS)
TrainingDatapath_B = '{}/{}/TrainingClasses_B/'.format(BASE_DIR, EXPERIMENT_NAME)  # Data from 2nd data source (Phase 3 Plant B data @ 15FPS)
MainPath           = '{}/{}/ModelOutputs/'.format(      BASE_DIR, EXPERIMENT_NAME) # Subfolder and path pointing to the where the trained model should be saved.


PickledTraining    = False   # Pickle or process raw data as input
TrainingDim        = 32      # X AND Y size of the input images. Phase 3 and 2 used image size of 32 --- a SIGNIFICANT resolution decrease from the input ~640x480 images.
TemporalWindow     = 15      # Defines how many video frames in each sequence. Phase 2 and 3 used 15, corresponding to 1 second's worth of 15FPS frames.
TrainingRatio      = 0.6     # The ratio of training to validation frames. WARNING: This code does not ensure a consistent train:val ratio across all classes, only across each of the two data sets.
                             # I.e. 40/60 of Plant A + 40/60 of Plant B. This can be a huge source of performance variation as different shuffles give different per-class train:val ratios.
                             # This becomes less and less of a problem the more data you have in each class --- a huge problem when you have only a few examples in a class.
Colormode          = True    # Use colored frames or grayscale them-- however we never used grayscaling for Phase 2 or 3. Recommend removing this functionality.
if Colormode:
    Channel        = 3
else:
    Channel        = 1

ClassesCount       = 0

random.seed(RANDOM_SEED) # Initialize random to use our given RANDOM_SEED for deterministic results
                         # We are still unsure how much variation we can expect due to random processes

# Make sure the directories exist before we get started:
if os.path.isdir(MainPath) == False:
    raise ValueError(" Directory {} does not exist-- model output will fail. Did you forget to make it?".format(MainPath))
    

if os.path.isdir(TrainingDatapath_A) == False:
    raise ValueError(" Directory {} does not exist-- dataloading will fail. Did you forget to make it?".format(TrainingDatapath))



def NaturalSort(list):
    """ Sorts the given iterable in the way that is expected.
    Intended to solve the issue where files, named sequentially, are not
    read-in sequentially by os.walk.

    Required arguments:
    list -- The iterable to be sorted.

    Adopted from:
    https://arcpy.wordpress.com/2012/05/11/sorting-alphanumeric-strings-in-python/

    """
    convert      = lambda text: int(text)   if  text.isdigit() else text
    alphanum_key = lambda key:  [convert(c) for c in re.split('([0-9]+)', key)]

    #return l.sort(key=int)
    return sorted(list, key = alphanum_key)

def ShuffleImages(sequences):

    '''Shuffles a list of sequences of iamges. Assumes that you are shuffle a list of (sequence, label) pairs
        '''

    n_sequences = len(sequences)

    Permutation = random.sample(range(0, n_sequences), n_sequences)

    ShuffledImageList = [sequences[i] for i in Permutation]

    # for i in Permutation:
    #     ShuffledImageList.append(sequences[i]])
        
    return ShuffledImageList

def ShuffleArraysTogether(data, labels):

    ''' Shuffles together a list of data + with a list of labels.
     A permutation is first calculated, then both arrays are shuffled
     using this same permutation. Arrays are assumed to have the same
     length along the first axis.
    '''

    permutation = np.random.permutation(len(labels))

    data   = data[permutation]
    labels = labels[permutation]

    return data, labels

def ShuffleSplitData(X_train, y_train, X_test, y_test):
    ''' Shuffles data which has already been split into training and validation sets.
    '''

    X_train, y_train = ShuffleArraysTogether(X_train, y_train)
    X_test,  y_test  = ShuffleArraysTogether(X_test, y_test)

    return X_train, y_train, X_test, y_test

def SplitData(sequences, TrainingRatio = 0.6):

    '''Extract training and testing data and labels given the 
    list of data sequences provided by DataReader.
    
    sequences     : list of tuples, each having two entires
                        first element  : an array of size (TemporalWindow, TrainingDim, TrainingDim, n_Channels)
                            comprising a single temporal image sequence (1 second of RGB video frames)
                        second element : Index indentifying the class of the temporal sequence in the first element.
    TrainingRatio : float. the desired ratio of training to validation data. Will be split wholesale, not per class. 
    '''

    # inferring relevant sizes of the data, and output size for training and validation
    n_sequences     = len(sequences)
    sequences       = ShuffleImages(sequences)
    TemporalWindow  = sequences[0][0].shape[0]
    TrainingDim     = sequences[0][0].shape[1]
    Channel         = sequences[0][0].shape[3]
    TrainLen        = int(n_sequences*TrainingRatio)
    TestLen         = n_sequences - TrainLen -1

    # Extracting training and validation (Test) data
    #########
    # WARNING: Assumes that you've already shuffled the data!
    #########  This is NOT a stratified split-- blind to class labels.
    TrainingSeg     = sequences[:TrainLen]
    TestSeg         = sequences[TrainLen+1:]

    TrainShape      = [-1, TemporalWindow, TrainingDim, TrainingDim, Channel]
    TestShape       = [-1,  TemporalWindow, TrainingDim, TrainingDim, Channel]


    TrainingData    = np.array([row[0] for row in TrainingSeg])
    TestingData     = np.array([row[0] for row in TestSeg])
    TrainingLabels  = np.array([row[1] for row in TrainingSeg])
    TestingLabels   = np.array([row[1] for row  in TestSeg])


    TrainingData    = TrainingData.reshape(    TrainShape)
    TestingData     = TestingData.reshape(     TestShape)
    TrainingLabels  = TrainingLabels.reshape( [TrainLen, 1])
    TestingLabels   = TestingLabels.reshape(  [TestLen,  1])

    return TrainingData, TrainingLabels, TestingData, TestingLabels



def DataReader(TrainingRatio, TrainingDatapath):

    '''Read data from the given path and pickle them or unpickle existing data.
        the function returns training and testing data and training and testing labels shuffled
        and divided as the training ratio.
        
        TrainingRatio    : The desired ratio of training to validation sequences. 
        TrainingDatapath : Folder containing the training frames. Assumes a structure of:
                            Training / (Class 1, Class 2, ... Class N) / (Clip 1, ... , Clip N) / (Frame1.jpg, ..., FrameNNN.jpg) 
                            '''

    # Loaded training data can be saved in an intermediate state, to speed up later runs
    # We use `.hdfs` files here instead of `.pickle`. `pickle` has some known security vulnerabilities.
    # `.hdfs` also seems to allow more flexibility in how the data is accessed and saved, although at present we do not use all of these
    # the package `hickle` allows us to interact with `.hdfs` as we would with `pickle`

    if PickledTraining and os.path.isfile(MainPath + 'data.hkl'):
        # Read from file if it exists-- WARNING: confirm that the .hkl being read is the one you think it is.
        print('Reading Hickle')

        sequences = hickle.load(MainPath + 'data.hkl')

        return SplitData(sequences)

    else:
        # If not reading from a previous run, we'll load the data in from scratch
        # If desired, this will be saved later as a new .hlk file from the desired training data.

        sequences       = []
        Tempimage_list  = np.zeros([TemporalWindow, TrainingDim, TrainingDim, 3])
        ClassIx         = 0
        CurrentClassCtr = 0

        # Loop through the training data directory, across all the main class subfolders, and their respective video clip subfolders.
        # Assumes training video clips are converted to `.JPG`s, and that these have already been carefully refined down
        # to frames that are suitable for training. We will almost never use an entire video clip for training.

        for Class in sorted(os.listdir(TrainingDatapath)):

            # Loop within every class, searching for training sample folders. `Clear` will take the longest, due to a huge class imbalance.
            for subdir, dirs, files in os.walk(TrainingDatapath + Class + '/'):

                # During experimental training and re-training, we may end up with some class folders being empty-- skip those.
                # In phase 2, some classes are left with "placeholder data", with the udnerstanding the the client may one day contribute real examples.
                if len(files) > 0:
                    # for every picture
                    #############
                    #WARNING: Ensure 'files' is sorted in the appropriate order. os.walk tends not to do what we expect/hope.
                    ######### Reading files in an arbitrary order means that our model may not be temporal at all.
                    if SORT:
                        files = NaturalSort(files)

                    frame_counter = 0

                    for Singlefile in files:
                        filepath = os.path.join(subdir, Singlefile)
                        img = cv2.imread(filepath)                      
                        if not Colormode:                               # This has never been used. ALl of Phase 2 and Phase 3 results used RGB inputs.
                            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # Kept only for the case that one desires grayscale testing in the future.
                                                                        

                        img = cv2.resize(img, (TrainingDim, TrainingDim), interpolation=cv2.INTER_NEAREST) # the required preprocessing

                        img = img.astype('float32') / 255.0

                        img = np.reshape(img,[1, TrainingDim, TrainingDim, 3])


                        if frame_counter < TemporalWindow:
                            # append until we fill the temporal window size
                            Tempimage_list[frame_counter,:,:,:] = img
                            frame_counter += 1 

                        else:
                            SingleSample = Tempimage_list.copy()
                            sequences.append([SingleSample, ClassIx])

                            # when the temporal window is full, shift backward and change the last index with a new frame
                            Tempimage_list           = np.roll(Tempimage_list, -1, axis=0)
                            Tempimage_list[-1,:,:,:] = img

                            CurrentClassCtr += 1   # counter to know how many pictures per class we have, it is helpful while we had unbalanced classes by the time of this development

                        if len(files) == TemporalWindow:   #Safety check in case the number if frames is exactly 15
                                SingleSample = Tempimage_list.copy()
                                sequences.append([SingleSample, ClassIx])
                                CurrentClassCtr += 1  

            print('Reading ' + Class +' With ' + str(CurrentClassCtr) + ' items.')
            CurrentClassCtr = 0

            #Tempimage_list = [] # flush the temporal window after the current smaple directory is over
            ClassIx += 1 # going to the next class (next directory)
        #ClassesCount = ClassIx
        if PickledTraining:
            # when all the loops are done, we can save the hickle
            hickle.dump(sequences, MainPath + "data.hkl", "w")

        # after pickling, prepare data for the training - this code is similar to the code in the first if statement
        return SplitData(sequences)

def post_split_class_count(y_train, y_test, csv_path = MainPath + "class_count_total.csv"):

    '''Provides a count of how many examples are in each class, following the training:validation split
        and shuffling. This provides an estimate of the degree to which training:val ratio, class by class,
        is sensitive to random shuffling. In cases of low example counts per class, it becomes more and more likely
        that all examples from a given class end up in only one of training or validation.

        y_train  : Training labels
        y_test   : Validation labels
        csv_path : INput where the count results will be saved.'''

    df_train                   = pd.DataFrame( {"y_train": [int(i[0]) for i in y_train] }     )
    df_test                    = pd.DataFrame( {"y_test" : [int(i[0]) for i in y_test ] }     )
    class_ids_df               = pd.concat(    [df_train, df_test], ignore_index=True, axis=1 )
    class_ids_df.columns       = ["y_train", "y_test"]
    
    class_counts_df            = pd.DataFrame()
    class_counts_df["y_train"] = class_ids_df["y_train"].value_counts().sort_index()
    class_counts_df["y_test" ] = class_ids_df["y_test" ].value_counts().sort_index()  

    print(class_counts_df)

    class_counts_df.to_csv(csv_path) 

def TwoSourceDataReader(TrainingRatio, TrainingDatapath_A, TrainingDatapath_B):

    ''' Reads data from two sources, ensuring that the desired training:val ratio
    is applied to both. Calls DataReader twice -- once for A data, once again for B
    Then concatenate the respective val and training sets.
    Finally, shuffle the combined A+B training and val sets.
    Run the model as usual.
    '''

    X_train_A, y_train_A, X_test_A, y_test_A = DataReader(TrainingRatio, TrainingDatapath_A)
    X_train_B, y_train_B, X_test_B, y_test_B = DataReader(TrainingRatio, TrainingDatapath_B)

    post_split_class_count(y_train_A, y_test_A, csv_path = MainPath + "class_count_PlantA.csv")
    post_split_class_count(y_train_B, y_test_B, csv_path = MainPath + "class_count_PlantB.csv")

    X_train = np.concatenate((X_train_A, X_train_B))
    y_train = np.concatenate((y_train_A, y_train_B))
    X_test  = np.concatenate((X_test_A , X_test_B ))
    y_test  = np.concatenate((y_test_A , y_test_B ))

    X_train, y_train, X_test, y_test = \
        ShuffleSplitData(X_train, y_train, X_test, y_test)


    return X_train, y_train, X_test, y_test

def CreateInputs(TemporalWindow, TrainingDim, Colormode = True):
    
    '''Helper function which initializes the model's input layers. In our case, there will be one layer for each frame in a given sequence.
    I.e. n_inputs = TemporalWindow = Video FPS'''

    if Colormode:

        input_img = [Input(shape = (TrainingDim, TrainingDim, 3)) for i in range(TemporalWindow)]

        return input_img

    else:

        input_img = [Input(shape = (TrainingDim, TrainingDim, 1)) for i in range(TemporalWindow)]

        return input_img

def CreateInitialReceptor(input_img_1):

    '''Helper function instantiating the initial receptor structure in the Inception architecture
        '''

    tower_1_1 = Conv2D(64, (1,1), padding='same', activation='relu')(input_img_1)
    tower_1_1 = Conv2D(64, (3,3), padding='same', activation='relu')(tower_1_1)

    tower_1_2 = Conv2D(64, (1,1), padding='same', activation='relu')(input_img_1)
    tower_1_2 = Conv2D(64, (5,5), padding='same', activation='relu')(tower_1_2)

    tower_1_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(input_img_1)
    tower_1_3 = Conv2D(64, (1,1), padding='same', activation='relu')(tower_1_3)

    output_1 = keras.layers.concatenate([tower_1_1, tower_1_2, tower_1_3], axis = 3)

    return output_1

def CreateMainReceptor(input_img_1, input_img_2):

    ''' Helper function instantiating the Main receptor structure in the Inception architecture.
        Will be repeated per each input in the sequence, i.e. the number of main recepter units
        will be equal to the number if frames in 1 second (15 in the case of SNT Phase 2 and 3).

        Input dimensions, padding, activation functions are simply as prescribed by the Temporal Inception literature.
        Thus these are hardcoded at the moment, and not experimented with. 
        '''

    tower_2_1 = Conv2D(64, (1,1), padding='same', activation='relu')(input_img_2)
    tower_2_1 = Conv2D(64, (3,3), padding='same', activation='relu')(tower_2_1)

    tower_2P = Conv2D(64, (3,3),    padding='same', activation='relu')(input_img_1)
    tower_2MP = MaxPooling2D((3,3), strides=(1,1), padding='same')(tower_2P)

    tower_2_2 = Conv2D(64, (1,1), padding='same', activation='relu')(input_img_2)
    tower_2_2 = Conv2D(64, (5,5), padding='same', activation='relu')(tower_2_2)

    tower_2_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(input_img_2)
    tower_2_3 = Conv2D(64, (1,1), padding='same', activation='relu')(tower_2_3)

    output_2 = keras.layers.concatenate([tower_2_1, tower_2MP, tower_2_2, tower_2_3], axis = 3)

    return output_2


## Execute dataloading and training steps.

def Train(lrate = 0.001):

    # Load data from Plants A and B, and split them respectively
    # Do not apply splitting on the combined A and B dataset.

    X_train, y_train, X_test, y_test = \
        TwoSourceDataReader(TrainingRatio, TrainingDatapath_A, TrainingDatapath_B)

    post_split_class_count(y_train, y_test)

    print ('Loading '+ str(len(y_train)) + ' training samples and ' + str(len(y_test)) + ' testing samples.')
    y_train = np_utils.to_categorical(y_train)
    y_test  = np_utils.to_categorical(y_test)

    print('Creating Model')
    # The Phase 2 (Case_A) model is 15 inputs, corresponding to 1 second of 15 FPS video.
    # Each input receives an RGB image (grayscale option available, but yet unused)
    input_img   = CreateInputs(TemporalWindow, TrainingDim, Colormode)

    ## Define model architecture:
    outputs_inc = []
    # First receptor 
    outputs_inc.append(CreateInitialReceptor(input_img[0]))

    # Subsequent receptors 
    for i in range(1,TemporalWindow):
        outputs_inc.append(CreateMainReceptor(input_img[i-1], input_img[i]))

    outputs_inc = keras.layers.concatenate(outputs_inc, axis = 3)
    conv_final1 = Conv2D(64, (1,1), padding = 'same', activation = 'relu')(outputs_inc)
    conv_final2 = Conv2D(64, (1,1), padding = 'same', activation = 'relu')(conv_final1)
    flatten     = Flatten()(conv_final2)
    dense       = Dense(9, activation = 'softmax')(flatten)
    model       = Model(inputs = input_img, outputs = dense)

    print (model.summary())

    # the model hyper parameters
      # the learnnig rate
    decay = lrate/N_EPOCHS # the decay rate

    if OPTIMIZER == 'SGD':
        optimizer = SGD(lr=lrate, momentum=0.9, decay=decay, nesterov=False)  # the optimizer

    elif OPTIMIZER == 'Adam':
        optimizer = keras.optimizers.Adam(lr=lrate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=decay, amsgrad=False)  # the optmizer
        
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

    print('Training Model')

    history = model.fit(
        [X_train[:,i] for i in range(TemporalWindow)],
        y_train, 
        validation_data = ([X_test[:,i] for i in range(TemporalWindow)], y_test),
        epochs          = N_EPOCHS, 
        batch_size      = BATCH_SIZE)

    ### Save training progress information
    # list all data in history
    print(history.history.keys())
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.savefig(MainPath + 'model accuracy.png')
    # summarize history for loss

    dict_keys(['val_loss', 'val_accuracy', 'loss', 'accuracy'])

    plt.clf()

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.savefig(MainPath + 'model loss.png')

    #maybe there will be web interface one day
    model_json = model.to_json()
    with open(MainPath + "model.json", "w") as json_file:
        json_file.write(model_json)

    model.save_weights(MainPath  + 'weightsmodel.h5')
    model.save(MainPath  + 'Puremodel.h5')
    os.system('cp Train.py {}Train.py'.format(MainPath))

Train()
