# Overview
SNTの学習・推論を行うために必要なコードのみをまとめました

## Description

### ◎CLSTM_AE(異常検知)
```
python clstmae.py
```
 - CLSTM_AEの学習を行います
 - 入力にはnpyファイルを用いて下さい(npyファイルは"/preprocessing/FramesToNpy.py"で作成出来ます)

### ◎Temporal-Inception(分類)
```
python Train.py
```
 - Temporal-Inceptionの学習を行います
 - 入力にはjpgファイル(動画をフレーム毎に分けた画像)を用いてください(指定ファイルは"/preprocessing/VidToFrame.py"で作成出来ます)

### ◎preprocessing
#### VidToFrame.py
 - 動画ファイルをフレーム毎に切り分けます(1秒あたり15フレーム)
#### FramesToNpy.py
 - フレーム画像をnpyファイルに変換します

### ◎AE + Inception
```
python online_viewer.py
```
 - CLSTMとTemporal-Inceptionモデルをそれぞれ用いて推論を行います。

#### snt_gpu.yml
 - 必要なパッケージ、モジュールが記載されています

## Example
 - 蝋燭動画出力例(https://drive.google.com/drive/u/0/folders/1RKcmNY9H_YNp5GP23BhxJLNzCU5bpOZt)